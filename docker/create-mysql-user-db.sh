#!/bin/bash

ECHO "Creating kamper database and user."
--Creating database and user.

CREATE DATABASE kamper;
CREATE USER 'kamper'@'%' IDENTIFIED BY 'kamper';
GRANT ALL PRIVILEGES ON * . * TO 'kamper'@'%';
