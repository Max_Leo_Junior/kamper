#!/bin/bash
if [ -z "$1" ]; then
  echo "&1, Container Name is required."
  return
fi
if [ -z "$2" ]; then
  echo "&2, Container Port is required.."
  return
fi
if [ -z "$3" ]; then
  echo "&3, Mysql root password is required."
  return
fi
echo "Creating docker database: $1, Port: $2, Password: *******"
sudo docker run -p $2:3306 --name $1 -e MYSQL_ROOT_PASSWORD=$3 -d mysql:latest


