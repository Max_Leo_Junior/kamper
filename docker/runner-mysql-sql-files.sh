#!/bin/bash
if [ -z "$1" ]; then
  echo "&1, Mysql Host is required."
  return
fi
if [ -z "$2" ]; then
  echo "&2, Mysql user name is required."
  return
fi
if [ -z "$3" ]; then
  echo "&3, Mysql Port is required."
  return
fi
if [ -z "$4" ]; then
  echo "&4, Mysql database Name is required."
  return
fi
--Run scripts on mysql database
echo "SQL script Runner will start working......."
for index in ../db/*.sql; do
  echo "Executing sql file: $(basename "$index" )"
  mysql -h $1 -u $2 -P $3 -p $2 < $index
done
