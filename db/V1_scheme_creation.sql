DROP TABLE IF EXISTS `kamper`.`Subscriber`;
CREATE TABLE `kamper`.`Subscriber` (
  `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `description` VARCHAR(100) NOT NULL,
  `type` VARCHAR(20) NOT NULL COMMENT 'By Device Id, By Admins, By Number of sessions.',
  `whiteList` LONGTEXT NOT NULL,
  `blackList` LONGTEXT,
  `active` BIT(1) NOT NULL DEFAULT b'1',
  `createDate` datetime NOT NULL,
  `updateDate` datetime NOT NULL
);

DROP TABLE IF EXISTS `kamper`.`UserAccount`;
CREATE TABLE `kamper`.`UserAccount` (
  `id` INT(11) AUTO_INCREMENT PRIMARY KEY,
  `token` VARCHAR(100) NOT NULL,
  `employeeId` INT(11) NOT NULL,
  `role` VARCHAR(20) NOT NULL COMMENT 'Values: ADMIN, ROLE_USER',
  `userName` VARCHAR(20) NOT NULL,
  `password` VARCHAR(20) NOT NULL,
  `active` BIT(1) NOT NULL DEFAULT b'1',
  `externalId` VARCHAR(50) COMMENT 'Hardware marker Id.',
  `createDate` datetime NOT NULL,
  `updateDate` datetime NOT NULL
);

DROP TABLE IF EXISTS `kamper`.`Sesions`;
CREATE TABLE `kamper`.`Sesions` (
  `id` INT(11) AUTO_INCREMENT PRIMARY KEY,
  `userId` INT(11) NOT NULL,
  `token` VARCHAR(50) NOT NULL,
  `sessionDate` datetime NOT NULL,
  `logoutDate` datetime NOT NULL,
  `createDate` datetime NOT NULL,
  `updateDate` datetime NOT NULL
);

DROP TABLE IF EXISTS `kamper`.`Employee`;
CREATE TABLE `kamper`.`Employee` (
  `id` INT(11) AUTO_INCREMENT PRIMARY KEY,
  `userId` INT(11) NOT NULL,
  `fullName` VARCHAR(100) NOT NULL,
  `positionName` VARCHAR(100) NOT NULL,
  `documentNumber` VARCHAR(100) NOT NULL,
  `phoneNumber` VARCHAR(100),
  `email` VARCHAR(100),
  `referenceName` VARCHAR(100),
  `referencePhone` VARCHAR(100),
  `hobbies` VARCHAR(100),
  `address` VARCHAR(100),
  `birthDate` DATE ,
  `createDate` datetime NOT NULL,
  `updateDate` datetime NOT NULL
);

DROP TABLE IF EXISTS `kamper`.`Assistance`;
CREATE TABLE `kamper`.`Assistance` (
  `id` INT(11) AUTO_INCREMENT PRIMARY KEY,
  `userId` INT(11) NOT NULL,
  `eventType` VARCHAR(20) NOT NULL COMMENT 'Values: Entrada, Salida.',
  `createDate` datetime NOT NULL,
  `updateDate` datetime NOT NULL
);

DROP TABLE IF EXISTS `kamper`.`AssistanceConsolidate`;
CREATE TABLE `kamper`.`AssistanceConsolidate` (
  `id` INT(11) AUTO_INCREMENT PRIMARY KEY,
  `userId` INT(11) NOT NULL,
  `type` VARCHAR(20) NOT NULL COMMENT 'VAlues: DAILY, MONTHLY',
  `dateRow` DATE NOT NULL COMMENT 'Values: DAY_OF_YEAR, MONTH_OF_YEAR',
  `validHours` DECIMAL(10, 2) NOT NULL COMMENT 'Amount of work hours.',
  `createDate` datetime NOT NULL,
  `updateDate` datetime NOT NULL
);

DROP TABLE IF EXISTS `kamper`.`AssistanceFiles`;
CREATE TABLE `kamper`.`AssistanceFiles` (
  `id` INT(11) AUTO_INCREMENT PRIMARY KEY,
  `processDate` datetime NOT NULL,
  `filePath` VARCHAR(1000) NOT NULL COMMENT 'S3 Url where file exist.',
  `processStatus` VARCHAR(20) NOT NULL COMMENT 'Value: STARTED, PROCESSING, DONE',
  `createDate` datetime NOT NULL,
  `updateDate` datetime NOT NULL
);