#!/usr/bin/env bash
if [ -z "$1" ]; then
  echo "[1]: VPC Id is required."
  return
fi
if [ -z "$2" ]; then
  echo "[2]: DB Subnet name is required."
  return
fi
JSON_AWS=$(aws ec2 describe-subnets --filters "Name=vpc-id,Values=$1" --output json)
SUBNET_IDS=$(jq -jr '.[] | .[] | " ", .SubnetId' <<< "${JSON_AWS}")
SUBNET_IDS=${SUBNET_IDS:1}

echo "${JSON_AWS}"
echo ${SUBNET_IDS}
echo "Creating DB subnet group. GroupName: $1, SubnetIds: ${SUBNET_IDS}."

JSON_AWS_SUBNET=$(  \
aws rds create-db-subnet-group \
--db-subnet-group-name $2 \
--db-subnet-group-description $2 \
--subnet-ids ${SUBNET_IDS}  \
--output json \
)

echo "${JSON_AWS_SUBNET}"
