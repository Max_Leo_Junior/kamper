#!/bin/bash
if [ -z "$1" ]; then
  echo "[1], Database name parameters is required."
  return
fi
if [ -z "$2" ]; then
  echo "[2], Database user parameters is required."
  return
fi
if [ -z "$3" ]; then
  echo "[3], Database password parameters is required."
  return
fi
if [ -z "$4" ]; then
  echo "[4], DB Subnet Group name is required."
  return
fi
if [ -z "$5" ]; then
  echo "[5], Security group Id is required."
  return
fi
if [ -z "$6" ]; then
  echo "[6], VPC id is required."
  return
fi

echo "Creating DB subnet group"
. create-db-subnet-group.sh $6 $4

echo "Creating kamper database name: $1."
echo "Creating kamper database with user: $2."
echo "Setting password for above configuration."

aws rds create-db-cluster --db-cluster-identifier $1 --engine aurora --engine-version 5.6.10a \
--engine-mode serverless --scaling-configuration MinCapacity=2,MaxCapacity=2,SecondsUntilAutoPause=600,AutoPause=true \
--master-username $2 --master-user-password $3 \
--db-subnet-group-name $4 --vpc-security-group-ids $5


